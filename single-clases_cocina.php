<?php get_header(); ?>

<main class="container">
    <?php while ( have_posts() ) : the_post();
    
        get_template_part('template-parts/content', 'post');
        //printf('<pre>%s</pre>', var_export( get_post_custom( get_the_ID() ), true ) );
    ?>

    <div class="more-info mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="include mb-5">
                    <h2 class="text-center separator">¿Que Incluye?</h3>
                    <ul class="list-group text-light">
                        <?php
                            $includes = get_post_meta( get_the_ID(), 'edc_class_include', true);
                            foreach ($includes as $include):
                        ?>
                        <li class="list-group-item"><?php echo $include ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="info-extra mb-5">
                    <h2 class="text-center separator">Información extra</h3>
                    <ul class="list-group text-light">
                        <li class="list-group-item font-weight-bold"><?php echo get_post_meta( get_the_ID(), 'edc_class_quotas', true) ?> cupos disponibles</li>
                        <li class="list-group-item"><span class="font-weight-bold">Costo del curso:</span> <span class="font-italic"><?php echo get_post_meta( get_the_ID(), 'edc_class_price', true) ?></span></li>
                        <li class="list-group-item"><span class="font-weight-bold">Fecha de inicio:</span> <span class="font-italic"><?php echo get_post_meta( get_the_ID(), 'edc_class_textdate_start', true) ?></span> <span class="font-weight-bold">Fecha de fin:</span> <span class="font-italic"><?php echo get_post_meta( get_the_ID(), 'edc_class_textdate_end', true) ?></span></li>
                        <li class="list-group-item"><span class="font-weight-bold">Horario:</span> <span class="font-italic"><?php echo get_post_meta( get_the_ID(), 'edc_class_time_start', true) .' - '. get_post_meta( get_the_ID(), 'edc_class_time_end', true) ?> hrs</span></li>
                        <li class="list-group-item"><span class="font-weight-bold">Días:</span> <span class="font-italic"><?php echo get_post_meta( get_the_ID(), 'edc_class_indic_days', true) ?></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="instructor text-center">
                    <h2 class="separator">Instructor</h3>
                    <?php
                        $instructorIds = get_post_meta( get_the_ID(), 'edc_class_instructor', true);
                        $args = array(
                            'post_type' => 'chefs',
                            'post__in'  => $instructorIds,
                            'posts_per_page' => 5
                        );
                        $instructor = new WP_Query($args);
                        while( $instructor->have_posts() ): $instructor->the_post();
                    ?>
                        <div class="row justify-content-center">
                            <div class="col-8 col-md-6">
                                <?php the_post_thumbnail( 'instructor', array('class' => 'img-fluid rounded-circle mb-4') ); ?>
                            </div>
                        </div>
                    <h3 class="text-primary"><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>     
    </div>    
</main>

<?php
    endwhile;
    get_footer();