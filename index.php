<?php get_header(); ?>

<div class="container mb-4 blog">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <blockquote class="subtitle text-center pl-3">
                    <?php
                        $id_blog = get_option('page_for_posts');
                        echo get_post_meta($id_blog, 'edc_blog_slogan_blog', true);
                    ?>
                </blockquote>
            </div>
        </div>
        <div class="row">
            <main class="col-md-8 col-lg-9">
                <h1 class="separator text-center">Nuestro Blog</h1>
                <?php while(have_posts()): the_post(); ?>
                    <div class="row mb-4 node">
                        <div class="col-md-4">
                            <?php the_post_thumbnail('mediano', array('class' => 'img-fluid') ); ?>
                        </div>
                        <div class="col-md-8">
                            <div class="body-node pt-4 pt-md-0">
                                <a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
                                <?php get_template_part('template-parts/meta', 'post'); ?>
                                <p><?php echo wp_trim_words(get_the_content(), 20, '.'); ?></p>
                                <a href="<?php the_permalink() ?>" class="btn btn-primary">Ver Entrada</a>
                            </div>
                        </div>
                    </div><!--.Node-->
                <?php endwhile; ?>

                <ul class="pagination mt-5 justify-content-center">
                    <li class="page-item">
                        <?php previous_posts_link( '< anteriores' ); ?>
                    </li>
                    <li class="page-item">
                        <?php next_posts_link( 'Siguientes >' ); ?>
                    </li>
                </ul>
            </main><!--.main-->
            <?php get_sidebar(); ?>
        </div><!--.row-->
    </div><!--.container-->

<?php get_footer();