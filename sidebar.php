<aside class="col-md-4 col-lg-3 p-4 bg-primary">

    <?php
        if( !is_active_sidebar('sidebar_widget')){
            return;
        }
        dynamic_sidebar('sidebar_widget');
    ?>
    <!-- <h2 class="text-center text-light separator inverso">Próximos Cursos</h2>
    <div class="proximosCursos">
        <div class="card">
            <img src="img/clase1.jpg" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <div class="card-footer">
                <a href="#">Más información.</a>
            </div>
        </div>.card-->
        <!--<div class="card">
            <img src="img/clase1.jpg" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <div class="card-footer">
                <a href="#">Más información.</a>
            </div>
        </div>.card
    </div> -->
</aside><!--.aside-->