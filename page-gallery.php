<?php
/*
 * Template Name: Página de galería
 */

get_header(); ?>

<main class="container">
    <?php while ( have_posts() ) : the_post();
    
        get_template_part('template-parts/content', 'page');

        //printf('<pre>%s</pre>', var_export( get_post_custom( get_the_ID() ), true ) );

        $gallery = get_post_meta(get_the_ID(), 'edc_gallery_file_list', true);
        ?>

        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card-columns">
                    <?php 
                        foreach ($gallery as $id => $imageUrl):
                        $imgFormat = wp_get_attachment_image_url($id, 'full') ;
                    ?>
                    <div class="card">
                        <a href="#" data-toggle="modal" data-target="#modalImage"><img src="<?php echo $imgFormat ?>" class="card-img-top img-fluid"></a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <img src="#" class="card-img-top img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endwhile; ?>
</main>

<?php get_footer();