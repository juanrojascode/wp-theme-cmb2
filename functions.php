<?php
/** Agregar CMB2 */
require_once dirname(__FILE__).'/cmb2.php';

/** Campos personalizados CMB2 */
require_once dirname(__FILE__).'/inc/custom-fields.php';

/** Agregar Post types */
require_once dirname(__FILE__).'/inc/posttypes.php';

/** Agregar Queries */
require_once dirname(__FILE__).'/inc/queries.php';

/** Agregar los widgets */
require_once dirname(__FILE__).'/inc/widgets.php';

/** Agregar Opciones del tema */
require_once dirname(__FILE__).'/inc/options-theme.php';

/** Imagenes Destacadas */
add_action('init','edc_image_thumbnails');
function edc_image_thumbnails($id){
    $image = get_the_post_thumbnail_url($id, 'full');
    $html = '';
    $bool = false;
    if ($image) {
        $bool = true;
        $html .= '<div class="row">';
        $html .= '<div class="col-12 imagen-destacada"></div>';
        $html .= '</div>';

        //Agregar estilo linealmente
        wp_register_style('custom', false);
        wp_enqueue_style('custom');

        //Creamos el css para el custom
        $image_thumbnail_css = "
            .imagen-destacada{
                background-image: url( {$image} );
            }
        ";
        wp_add_inline_style('custom', $image_thumbnail_css);
    }
    return array($html, $bool);
}

/**
 *  Ocultando el editor de texto para las páginas
 *  inicio y galería
 */
add_action( 'init', 'wprocs_custom_init' );
function wprocs_custom_init() {
    // Get the post ID on edit post
    $current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );

    if ($current_post_id==10 || $current_post_id==128) {
        remove_post_type_support( 'page', 'editor' );
    }
}

/**
 *  Carga Scripts y CSS
 */
function edc_scripts(){
    /** Agrega stilos */
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '4.4.1');
    wp_enqueue_style('style', get_stylesheet_uri(), array('bootstrap-css'));

    /** Agrega scripts */
    wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.js', array('jquery'), '1.0', true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('popper'), '4.4.1', true);
    wp_enqueue_script('custom-js', get_template_directory_uri() . '/js/custom.js', array(), '1', true);
}
add_action('wp_enqueue_scripts', 'edc_scripts');

/**
 * Funciones que cargan antes de activar el tema
 */
function edc_setup(){

    /** Habilita la edición del logo */
    add_theme_support('custom-logo');

    /** Activa los thumbnails */
    add_theme_support('post-thumbnails');

    /** Definir tamaños de imágenes */
    add_image_size('mediano', 510, 340, true);
    add_image_size('instructor', 410, 410, true);

    /** Menú de navegación */
    register_nav_menus(array(
        'menu_principal' => esc_html__('Menú Principal', 'escuelacocina')
    ));
}
add_action('after_setup_theme','edc_setup');
 
/**
 *  Filtros
 */
/** Agregando clase de bootstrap al menu principal */
add_filter('nav_menu_link_attributes', 'edc_link_class', 10, 3);
function edc_link_class($classes, $item, $args){
    if ($args->theme_location == 'menu_principal') {
        $classes['class'] = 'nav-link';
    }
    return $classes;
}

/** Agregando clase a links de paginación */
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');
function posts_link_attributes() {
  return 'class="page-link"';
}

/** Agregando estado personalizado en la pagina clases en el administrado en "páginas" */
add_filter('display_post_states', 'edc_display_state_class', 10, 2);
function edc_display_state_class($states, $post){
    if (
        ( 'page' == get_post_type($post->ID) ) && ( 'page-class.php' == get_page_template_slug($post->ID) )
        ) {
        $states[] = __('Página de clases <a href="edit.php?post_type=clases_cocina">Administrar clases</a>');
    }
    return $states;
}

add_action('widgets_init', 'edc_widget_sidebar');
function edc_widget_sidebar(){
    register_sidebar(array(
        'name'      => 'Widget lateral',
        'id'        => 'sidebar_widget',
        'before_widget' => '<div class="proximosCursos">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="text-center text-light separator inverso">',
        'after_title'   => '</h2>'
    ));
}