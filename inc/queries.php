<?php

function edc_query_cursos($cantidad = -1){
    $args = array(
        'post_type' => 'clases_cocina',
        'posts_per_page' => $cantidad
    );

    $class =  new WP_Query($args);

    while( $class->have_posts() ): $class->the_post();
    //printf('<pre>%s</pre>', var_export( get_post_custom( get_the_ID() ), true ) );
    ?>

    <div class="col-md-6 col-lg-4">
        <div class="card class">
            <?php the_post_thumbnail('mediano', array('class' => 'card-img-top img-fluid')) ?>
            <div class="data p-3 bg-primary d-flex justify-content-between align-items-center text-light">
                <div class="date">
                    <?php 
                        $date = get_post_meta(get_the_ID(), 'edc_class_textdate_start', true);
                        $time = get_post_meta(get_the_ID(), 'edc_class_time_start', true);
                        $price = get_post_meta(get_the_ID(), 'edc_class_price', true);
                    ?>
                    <p class="mb-0 font-weight-bold">Fecha: <span class="font-weight-normal"><?php echo $date; ?></span></p>
                    <p class="mb-0 font-weight-bold">Hora: <span class="font-weight-normal"><?php echo $time; ?></span></p>
                </div>
                <div class="price">
                    <span class="badge badge-secondary p-2">$ <?php echo $price ?></span>
                </div>
            </div>
            <div class="card-body">
                <h3><?php echo get_the_title(); ?></h3>
                <p class="class-description"><?php echo get_post_meta(get_the_ID(), 'edc_class_subtitle', true); ?></p>
                <p class="card-text"><?php echo wp_trim_words(get_the_content(), 20, '.'); ?></p>
                <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Más información</a>
            </div>
        </div>
    </div><!--.class-->

<?php
    endwhile;
    wp_reset_postdata();
}
