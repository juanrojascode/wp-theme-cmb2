<?php
/**
 *  Metaboxes para Home
 */


/**
 * Añade campos al editor de la página de incio
 */
add_action( 'cmb2_admin_init', 'edc_fields_homepage' );
function edc_fields_homepage() {
    $prefix = 'edc_home_';
    $id_home = get_option('page_on_front');
	/**
	 * Metabox to be displayed on a single page ID
	 */
	$edc_fields_homepage = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Principial | Home top', 'cmb2' ),
		'object_types' => array( 'page' ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array(
			'id' => array( $id_home ),
		), // Specific post IDs to display this metabox
    ) );
    
    $edc_fields_homepage->add_field( array(
		'name'    => esc_html__( 'Texto superior', 'cmb2' ),
		'desc'    => esc_html__( 'Texto para la parte superior del sitio web', 'cmb2' ),
		'id'      => $prefix . 'text_top_0',
		'type'    => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 5,
		),
    ) ); 

    $edc_fields_homepage->add_field( array(
		'name' => esc_html__( 'Imagen', 'cmb2' ),
		'desc' => esc_html__( 'Imagen posicionada a la derecha', 'cmb2' ),
		'id'   => $prefix . 'image_top_0',
		'type' => 'file',
    ) ); 
    
    $edc_fields_homepage->add_field( array(
		'name'    => esc_html__( 'Texto superior 2', 'cmb2' ),
		'desc'    => esc_html__( 'Texto para la parte superior del sitio web', 'cmb2' ),
		'id'      => $prefix . 'text_top_1',
		'type'    => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 5,
		),
    ) ); 
    
    $edc_fields_homepage->add_field( array(
		'name' => esc_html__( 'Imagen 2', 'cmb2' ),
		'desc' => esc_html__( 'Imagen posicionada a la izquierda', 'cmb2' ),
		'id'   => $prefix . 'image_top_2',
		'type' => 'file',
    ) ); 

    // New block
    $edc_fields_homepage_2 = new_cmb2_box( array(
		'id'           => $prefix . 'metabox_2',
		'title'        => esc_html__( 'Invitación | Post bottom', 'cmb2' ),
		'object_types' => array( 'page' ), // Post type
		'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array(
			'id' => array( $id_home ),
		), // Specific post IDs to display this metabox
    ) );

    $edc_fields_homepage_2->add_field( array(
		'name'    => esc_html__( 'Texto', 'cmb2' ),
		'desc'    => esc_html__( 'Texto a mostrar', 'cmb2' ),
		'id'      => $prefix . 'postbottom_text',
		'type'    => 'wysiwyg',
		'options' => array(
			'textarea_rows' => 5,
		),
    ) ); 

    $edc_fields_homepage_2->add_field( array(
		'name' => esc_html__( 'Imagen', 'cmb2' ),
		'desc' => esc_html__( 'Cargue aquí la imagen que irá de fondo', 'cmb2' ),
		'id'   => $prefix . 'postbottom_image',
		'type' => 'file',
    ) ); 
}

/**
 * Añade campos al Template "Página con iconos"
 */
add_action( 'cmb2_admin_init', 'edc_fields_page_icons' );
function edc_fields_page_icons() {
    $prefix = 'edc_page_icons_';
	/**
	 * Repeatable Field Groups
	 */
	$edc_fields_icons = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Iconos con descripción', 'cmb2' ),
        'object_types' => array( 'page' ),
        'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array(
            'key'      => 'page-template',
            'value'    => 'page-icons.php'
        ),
    ) );

    $edc_fields_icons->add_field( array(
		'name' => esc_html__( 'Título de la sección', 'cmb2' ),
		'desc' => esc_html__( 'Añada un título para la sección', 'cmb2' ),
		'id'   => $prefix . 'title_section',
		'type' => 'text',
	) );

	// $group_field_id is the field id string, so in this case: 'yourprefix_group_demo'
	$group_field_id = $edc_fields_icons->add_field( array(
		'id'          => $prefix . 'nosotros',
		'type'        => 'group',
		'description' => esc_html__( 'Agregue opciones según sea necesario', 'cmb2' ),
		'options'     => array(
			'group_title'    => esc_html__( 'Característica {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'     => esc_html__( 'Agergar otra', 'cmb2' ),
			'remove_button'  => esc_html__( 'Remover', 'cmb2' ),
			'sortable'       => true,
			// 'closed'      => true, // true to have the groups closed by default
			'remove_confirm' => esc_html__( '¿Seguro que quiere eliminar esta característica?', 'cmb2' ) // Performs confirmation before removing group.
		),
	) );

	/**
	 * Group fields works the same, except ids only need
	 * to be unique to the group. Prefix is not needed.
	 *
	 * The parent field's id needs to be passed as the first argument.
	 */
	$edc_fields_icons->add_group_field( $group_field_id, array(
		'name'       => esc_html__( 'Título', 'cmb2' ),
		'id'         => 'title_icon',
		'type'       => 'text',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );

	$edc_fields_icons->add_group_field( $group_field_id, array(
		'name'        => esc_html__( 'Descripción', 'cmb2' ),
		'description' => esc_html__( 'Agrega la descripción de la caracteristicas', 'cmb2' ),
		'id'          => 'description_icon',
		'type'        => 'textarea_small',
	) );

	$edc_fields_icons->add_group_field( $group_field_id, array(
		'name' => esc_html__( 'Imagen', 'cmb2' ),
		'id'   => 'image_icon',
		'type' => 'file',
	) );
}

/**
 * Añada campos al tipo de post "clases"
 */
add_action( 'cmb2_admin_init', 'edc_fields_class' );
function edc_fields_class() {
	$prefix = 'edc_class_';
	
	/** Bloque uno **/
	$edc_fields_class = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Información de las clases', 'cmb2' ),
        'object_types' => array( 'clases_cocina' ),
        'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true,
	) );

    $edc_fields_class->add_field( array(
		'name' => esc_html__( 'Subtitulo de la clase', 'cmb2' ),
		'desc' => esc_html__( 'Añada un subtitulo para la clase', 'cmb2' ),
		'id'   => $prefix . 'subtitle',
		'type' => 'text',
	) );

	$edc_fields_class->add_field( array(
		'name' => esc_html__( 'Precio', 'cmb2' ),
		'desc' => esc_html__( 'Añada el precio del curso', 'cmb2' ),
		'id'   => $prefix . 'price',
		'type' => 'text_money',
		'column' => true
		// 'before_field' => '£', // override '$' symbol if needed
		// 'repeatable' => true,
	) );

	$edc_fields_class->add_field( array(
		'name' => esc_html__( 'Número de cupos', 'cmb2' ),
		'desc' => esc_html__( 'Añada la cantidad máxima de cupos', 'cmb2' ),
		'id'   => $prefix . 'quotas',
		'type' => 'text',
	) );	

	$edc_fields_class->add_field( array(
		'name' => esc_html__( '¿Qué incluye?', 'cmb2' ),
		'desc' => esc_html__( 'Añada lo que viene incluido en el curso', 'cmb2' ),
		'id'   => $prefix . 'include',
		'type' => 'text',
		'repeatable' => true
	) );

	$edc_fields_class->add_field( array(
		'name'      	=> esc_html__( 'Instructor', 'cmb2' ),
		'desc'			=> esc_html__( 'Escriba el nombre del instrutor y seleccione', 'cmb2' ),
		'id'        	=> $prefix . 'instructor',
		'type'      	=> 'post_search_ajax',
		'limit'      	=> 10, 		// Limit selection to X items only (default 1)
		//'sortable' 	 	=> true, 	// Allow selected items to be sortable (default false)
		'query_args'	=> array(
			'post_type'			=> array( 'chefs' ),
			'post_status'		=> array( 'publish' ),
			'posts_per_page'	=> -1
		)
	) );

	/** Bloque dos **/
	$edc_fields_class_2 = new_cmb2_box( array(
		'id'           => $prefix . 'metabox_2',
		'title'        => esc_html__( 'Fecha y hora de la clase', 'cmb2' ),
        'object_types' => array( 'clases_cocina' ),
        'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true,
	) );

	$edc_fields_class_2->add_field( array(
		'name' => esc_html__( 'Indicadores de días', 'cmb2' ),
		'desc' => esc_html__( 'Añada las indicaciones de los días. Ej: Todos los sábados', 'cmb2' ),
		'id'   => $prefix . 'indic_days',
		'type' => 'text',
	) );

	$edc_fields_class_2->add_field( array(
		'name' => esc_html__( 'Fecha de inicio', 'cmb2' ),
		'desc' => esc_html__( 'Añada la fecha de inicio de la clase', 'cmb2' ),
		'id'   => $prefix . 'textdate_start',
		'type' => 'text_date',
		'date_format' => 'd-m-Y',
		'column' => true
	) );

	$edc_fields_class_2->add_field( array(
		'name' => esc_html__( 'Fecha de finalización', 'cmb2' ),
		'desc' => esc_html__( 'Añada la fecha de finalización de la clase', 'cmb2' ),
		'id'   => $prefix . 'textdate_end',
		'type' => 'text_date',
		'date_format' => 'd-m-Y',
		'column' => true
	) );

	$edc_fields_class_2->add_field( array(
		'name' => esc_html__( 'Hora de inicio', 'cmb2' ),
		'desc' => esc_html__( 'Añada la hora', 'cmb2' ),
		'id'   => $prefix . 'time_start',
		'type' => 'text_time',
		'time_format' => 'H:i', // Set to 24hr format
		'column' => true
	) );

	$edc_fields_class_2->add_field( array(
		'name' => esc_html__( 'Hora de fin', 'cmb2' ),
		'desc' => esc_html__( 'Añada la hora', 'cmb2' ),
		'id'   => $prefix . 'time_end',
		'type' => 'text_time',
		'time_format' => 'H:i', // Set to 24hr format
	) );
}

/**
 * Añada campos al tipo de post "clases"
 */
add_action( 'cmb2_admin_init', 'edc_fields_gallery' );
function edc_fields_gallery() {
	$prefix = 'edc_gallery_';
	
	$edc_fields_gallery = new_cmb2_box( array(
		'id'           => $prefix . 'metabox',
		'title'        => esc_html__( 'Agregar imágenes para la galería', 'cmb2' ),
        'object_types' => array( 'page' ),
        'context'      => 'normal',
		'priority'     => 'high',
		'show_names'   => true, // Show field names on the left
		'show_on'      => array(
            'key'      => 'page-template',
            'value'    => 'page-gallery.php'
        ),
    ) );

	$edc_fields_gallery->add_field( array(
		'name'         => esc_html__( 'Imágenes', 'cmb2' ),
		'desc'         => esc_html__( 'Carga o selecciona las imágenes que se mostrarán en la galería.', 'cmb2' ),
		'id'           => $prefix . 'file_list',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );
}

