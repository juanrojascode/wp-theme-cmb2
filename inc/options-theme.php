<?php

add_action( 'cmb2_admin_init', 'edc_options_theme' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function edc_options_theme() {

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'edc_theme_options',
		'title'        => esc_html__( 'Ajustes del theme "Escuela de cocina"', 'cmb2' ),
		'object_types' => array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'edc_theme_options', // The option key and admin menu page slug.
		'icon_url'        => 'dashicons-edit', // Menu icon. Only applicable if 'parent_slug' is left empty.
		'menu_title'      => esc_html__( 'Ajustes del tema', 'cmb2' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'cmb2' ), // The text for the options-page save button. Defaults to 'Save'.
		// 'disable_settings_errors' => true, // On settings pages (not options-general.php sub-pages), allows disabling.
		// 'message_cb'      => 'yourprefix_options_page_message_callback',
		// 'tab_group'       => '', // Tab-group identifier, enables options page tab navigation.
		// 'tab_title'       => null, // Falls back to 'title' (above).
		// 'autoload'        => false, // Defaults to true, the options-page option will be autloaded.
	) );

	/**
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */
	$cmb_options->add_field( array(
		'name'    => esc_html__( 'Color primario del tema', 'cmb2' ),
		'desc'    => esc_html__( 'Seleccione un color primario para el sitio web (enlaces, botones, fondo)', 'cmb2' ),
		'id'      => 'color-primary',
		'type'    => 'colorpicker',
		'default' => '#f46669',
    ) );
    
    $cmb_options->add_field( array(
		'name'    => esc_html__( 'Color secundario del tema', 'cmb2' ),
		'desc'    => esc_html__( 'Seleccione un color secundario para el sitio web (enlaces, botones, fondo)', 'cmb2' ),
		'id'      => 'color-secondary',
		'type'    => 'colorpicker',
		'default' => '#c7c57d',
    ) );
    
    $cmb_options->add_field( array(
		'name'    => esc_html__( 'Logotipo del tema', 'cmb2' ),
		'desc'    => esc_html__( 'Cargue una imagen de logotipo', 'cmb2' ),
		'id'      => 'logotipo',
		'type'    => 'file'
    ) );
    
    $cmb_options->add_field( array(
		'name'    => esc_html__( 'Separador del tema', 'cmb2' ),
		'desc'    => esc_html__( 'Cargue una imagen para el separador', 'cmb2' ),
		'id'      => 'separator',
		'type'    => 'file'
	) );

	$cmb_options->add_field( array(
		'name'             => esc_html__( '¿Cuantos cursos mostrar?', 'cmb2' ),
		'desc'             => esc_html__( 'Seleccione la cantidad de cursos a mostrar en Home (Es preferible un número pequeño para no sobre cargar el inicio)', 'cmb2' ),
		'id'               => 'radius-class-option',
		'type'             => 'radio_inline',
		'show_option_none' => '3 (Por defecto)',
		'options'          => array(
			'6' => esc_html__( '6', 'cmb2' ),
			'9'   => esc_html__( '9', 'cmb2' )
		),
	) );
}

add_action('wp_footer', 'edc_style_opctions');
function edc_style_opctions(){
    $options = get_option('edc_theme_options');

    $colorPrimary = $options['color-primary'];
    $colorSecondary = $options['color-secondary'];
	$separator = $options['separator'];

	if (!isset($separator)) {
		$separator = get_template_directory_uri().'/img/separador.png';
	}

	//Registrar css inline
	wp_register_style('custom-options-theme', false);
	wp_enqueue_style('custom-options-theme');

	//Creamos el css para el custom
	$data = "
		:root {
			--primario: {$colorPrimary};
			--secundario: {$colorSecondary};
		}
		.separator::after{
			background-image: url({$separator});
		}
    ";
	wp_add_inline_style('custom-options-theme', $data);
}