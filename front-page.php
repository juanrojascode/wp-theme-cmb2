<?php get_header();

    while (have_posts()): the_post();
    $id_page = get_the_ID();
?>

<main>
    <section class="container-fluid">
        <div class="row hr-100">
            <div class="col-md-6 bg-primary">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-sm-10 col-md-6">
                        <div class="content text-center text-light py-5">
                            <?php echo get_post_meta($id_page, 'edc_home_text_top_0', true); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 imagen-superior" style="background-image: url(<?php echo get_post_meta($id_page, 'edc_home_image_top_0', true); ?>)"></div>
        </div>
        <div class="row hr-100">
            <div class="col-md-6 bg-secondary order-md-1">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-sm-10 col-md-6">
                        <div class="content text-center py-5">
                        <?php echo get_post_meta($id_page, 'edc_home_text_top_1', true); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 imagen-inferior" style="background-image: url(<?php echo get_post_meta($id_page, 'edc_home_image_top_2', true); ?>)"></div>
        </div>
    </section>

    <section class="clases py-4 container-fluid">
        <?php
            $nosotros = new WP_Query('pagename=nosotros');
            while( $nosotros->have_posts() ): $nosotros->the_post();
                get_template_part('template', 'parts/icons');
            endwhile; wp_reset_postdata();
        ?>
    </section><!--.section-->

    <section class="clases py-4">
        <h1 class="text-center my-5 separator">Próximas Clases</h1>
        <div class="container">
            <div class="row">
                <?php
                    $options = get_option('edc_theme_options');
                    $value=3;
                    
                    if ( isset( $options['radius-class-option'] ) ) {
                        $value = (int) $options['radius-class-option'];   
                    }

                    edc_query_cursos($value);
                ?>
                <div class="more-class d-flex w-100 justify-content-end">
                    <a href="<?php echo get_permalink( get_page_by_title('Próximas Clases') ); ?>" class="btn btn-primary">Ver todas las clases</a>
                </div>
            </div><!--.row-->
        </div><!--.container-->
    </section><!--.section-->

    <section class="main-footer hr-100" style="background-image: url(<?php echo get_post_meta($id_page, 'edc_home_postbottom_image', true); ?>)">
        <div class="container h-100">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-md-10 text-center text-light">
                    <p><?php echo get_post_meta($id_page, 'edc_home_postbottom_text', true); ?></p>
                    <a href="<?php echo get_permalink(get_page_by_title('Contacto')->ID); ?>" class="btn btn-primary text-uppercase">Más información</a>
                </div>
            </div>
        </div>
    </section><!--.section-->
</main><!--.main-->

<?php 
    endwhile;
    get_footer();