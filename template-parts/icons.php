<?php
    //printf('<pre>%s</pre>', var_export( get_post_custom( get_the_ID() ), true ) );
?>
<div class="columns-3 my-5">
    <h2 class="text-center separator"><?php echo get_post_meta( get_the_ID(), 'edc_page_icons_title_section', true) ?></h2>
    <div class="row">
        <?php 
            $icons = get_post_meta( get_the_ID(), 'edc_page_icons_nosotros', true);
            foreach ($icons as $icon):
        ?>
            <div class="col-md-4 text-center">
                <img src="<?php echo $icon['image_icon'] ?>" class="img-fluid mb-3">
                <h3><?php echo $icon['title_icon'] ?></h3>
                <p><?php echo $icon['description_icon'] ?></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>