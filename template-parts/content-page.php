<?php 
    // guardamos respuesta arreglo con html y boolean
    $html = edc_image_thumbnails( get_the_ID() );
    echo $html[0];
?>
<div class="row justify-content-center">
    <div class="py-3 px-5 card page-description <?php echo $html[1] ? 'col-md-8 top-destacada' : 'col-md-12'; ?>">
        <h1 class="text-center my-5 separator"><?php the_title(); ?></h1>
        <?php the_content(); ?>
    </div>
</div>