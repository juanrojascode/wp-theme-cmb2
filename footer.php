<footer class="pt-5 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php
                        wp_nav_menu(array(
                            'menu_class' => 'nav d-flex flex-column flex-md-row text-center text-uppercase mb-4 mb-md-0',
                            'theme_location' => 'menu_principal'
                        ));
                    ?>
                </div>
                <div class="col-md-4 d-flex justify-content-center justify-content-md-end align-items-center">
                    <p class="text-center copyright">Todos los derechos reservados <?php echo date('Y') ?>.</p>
                </div>
            </div>
        </div>
    </footer><!--.footer-->

    <?php wp_footer(); ?>
</body>
</html>