<?php get_header(); ?>

<main class="container">
    <?php 
        while ( have_posts() ) : the_post();
    
            get_template_part('template-parts/content', 'post');
        
            if (comments_open() || get_comments_number() ){
                comments_template();
            }
            else{
                echo '<div class="row justify-content-center mt-5"><div class="col-md-8"><p class="text-center comments-closed alert alert-danger">Los comentarios están cerrados</p></div></div>';
            }
            
        endwhile;
    ?>
</main>

<?php get_footer();