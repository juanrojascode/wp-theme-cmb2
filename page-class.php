<?php 
/*
 * Template Name: Página con Clases
 */

get_header(); ?>

<div class="container mb-4">
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <!-- <blockquote class="subtitle text-center pl-3"> -->
                <?php
                    while ( have_posts() ) : the_post();
                
                        the_content();

                        $title = get_the_title();
                
                    endwhile;
                ?>
                <!-- </blockquote> -->
            </div>
        </div>
    </div>

    <section class="clases">
        <div class="container mb-4">
            <h1 class="text-center my-5 separator"><?php echo $title; ?></h1>
            <div class="row">
                <?php edc_query_cursos(); ?>
            </div><!--.row-->
        </div><!--.container-->
    </section>

</div>

<?php get_footer();