<?php 
/*
 * Template Name: Página con Iconos 
 */

get_header(); ?>

<main class="container">
    <?php while ( have_posts() ) : the_post();
    
        get_template_part('template-parts/content', 'page');

        get_template_part('template', 'parts/icons');
        
    endwhile; ?>
</main>

<?php get_footer();