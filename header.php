<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo get_the_title() . ' | ' . get_bloginfo()?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >

    <header class="header my-5">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-8 col-md-4">
                    <a href="<?php echo get_home_url('/'); ?>">
                    <?php
                        $opcions = get_option('edc_theme_options');
                        if (isset($opcions['logotipo'])):
                    ?>
                        <img src="<?php echo $opcions['logotipo']; ?>" class="img-fluid" alt="">
                    <?php else: ?>
                        <img src="<?php echo get_template_directory_uri() ?>/img/logo.svg" class="img-fluid" alt="">
                    <?php endif; ?>
                    </a>
                </div><!--.col-md-4-->
                <div class="col-md-8">
                    <nav class="navbar navbar-expand-lg navbar-light justify-content-center">
                        <button class="navbar-toggler mb-4 mb-md-0" type="button" data-toggle="collapse" data-target="#nav-principal" aria-controls="nav-principal" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <?php
                            wp_nav_menu(array(
                                'menu_class' => 'nav flex-column flex-md-row text-center justify-content-lg-end',
                                'container_id' => 'nav-principal',
                                'container_class' => 'collapse navbar-collapse justify-content-center justify-content-lg-end text-center text-uppercase',
                                'theme_location' => 'menu_principal'
                            ));
                        ?>
                    </nav>
                </div><!--.col-md-8-->
            </div><!--.row-->
        </div><!--.container-->
    </header><!--.header-->