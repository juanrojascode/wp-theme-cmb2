// Compatibility
$ = jQuery.noConflict();

$(document).ready(function( ) {
    /*
    *   Modal Image
    */
   $('[data-target="#modalImage"]').click(function (e) { 
        e.preventDefault();
        let ele = $(this).children().attr('src');
        $('#modalImage img').attr('src', ele)
    });
    
});