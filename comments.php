<div class="row justify-content-center mt-5">
    <div class="col-md-8">
        <?php comment_form(); ?>
    </div>
    <div class="col-md-8">
        <?php
            $counterComments = wp_count_comments();
            if ($counterComments->total_comments != 0):
        ?>
            <h2 class="text-center my-4">Comentarios</h2>
        <?php endif; ?>
        <ul class="comments-list alert list-unstyled">
            <?php
                $comments = get_comments(array(
                    'port_id'   => $post->ID,
                    'status'    => 'approve'
                ) );
                wp_list_comments(array(
                    'per_page'  => 10,
                    'reverse_top_level' => false
                ), $comments);
            ?>
        </ul>
    </div>
</div>